function defaultErrorFunction(){
}
function defaultDoneFunction(){
}
var header = $("meta[name='_csrf_header']").attr("content");
var token = $("meta[name='_csrf']").attr("content");
function post(theUrl, data,successFunction,errorFunction,doneFunction){
        $.ajax({
            scriptCharset: "utf-8",
			type : "POST",
			contentType : "application/json; charset=utf-8",
			url : theUrl,
			data : data,
			dataType : 'json',
			timeout : 100000,
			beforeSend: function(xhr){
                    xhr.setRequestHeader(header, token);
            },
			success : function(data) {
			    if(successFunction){
			        successFunction(data);
			    }
			},
			error : function(e) {
                if(errorFunction){
                    errorFunction(e);
                }
			},
			done : function(e) {
			    if(doneFunction){
			        doneFunction(e);
			    }
			}
		});
}

function post(theUrl, data,successFunction){
        $.ajax({
			type : "POST",
			contentType : "application/json",
			url : theUrl,
			data : data,
			dataType : 'json',
			timeout : 100000,
			beforeSend: function(xhr){
                    //xhr.setRequestHeader(header, token);
            },
			success : function(data) {
			    if(successFunction){
			        successFunction(data);
			    }
			},
			done : function(e) {

			}
		});
}
function send(theUrl, data){
    $.ajax({
        type : "POST",
        url : theUrl,
        data : data,
        dataType : 'string',
        timeout : 100000,
        beforeSend: function(xhr){
            //xhr.setRequestHeader(header, token);
        },

    });
}
(function ($) {
    $.fn.MySplit = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);

(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);

function reloadTable(tableId,url,columns){
    if ( $.fn.dataTable.isDataTable( tableId ) ) {
        var table =$(tableId).DataTable();
        table.ajax.reload();
    }
    else {
        $(tableId).DataTable( {
            "ajax": {
                "url": url,
                "dataSrc": "content"
            },
            "columns": columns
        } );
        $(tableId+' tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');
            } );
    }
}

function deleteRowTable(buttonId,tableId,columnId,deleteFunction){
    $(buttonId).click( function () {
            var table =$(tableId).DataTable();
            //alert( table.rows('.selected').data().length +' row(s) selected' );
            var data = table.rows('.selected').data();
            for(var i = 0; i<data.length; i++){
                //alert(data[i][columnId]);
                if(deleteFunction){
                    deleteFunction(data[i][columnId]);
                }
            }
    } );
}