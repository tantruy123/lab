<%--
  Created by IntelliJ IDEA.
  User: tan
  Date: 2016-10-30
  Time: 1:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<%@ page errorPage="error.jsp" %>

<script>
        //var jsonData = $.getJSON("url");
    </script>

<div class="container">
    <div style="margin-left: 130px;">Title : ${title}</div>
    <p></p>

    <script src="/resources/markdown/marked.js"></script>
    <script src="/resources/markdown/vue.js"></script>
    <script src="/resources/markdown/lodash.js"></script>
    <div class="container">
        <ul id="example">

        </ul>
        <form id="myForm">
            <div class="form-group">
                <div id="editor" style="margin-left: 120px;">
                    <h3>Markdown text editor</h3>
                    <textarea :value="input" @input="update" style="height: 800px;"  name="mytextarea" id="mytextarea"></textarea>
                    <div v-html="compiledMarkdown"></div>
                </div>
                <script>
                    new Vue({
                        el: '#editor',
                        data: {
                            input: "# ${content}"
                        },
                        computed: {
                            compiledMarkdown: function() {
                                return marked(this.input, {
                                    sanitize: true
                                })
                            }
                        },
                        methods: {
                            update: _.debounce(function(e) {
                                this.input = e.target.value
                            }, 1)
                        }
                    })
                </script>
                <div style="margin-top: 15px;margin-left: 120px;">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>

            </div>

        </form>
    </div>
    <script>
        $(document).ready(function() {
            $('#theNewForm').submit(function (e) {
                //    e.preventDefault();
                // send ajax
                var jsonData = $(this).serializeFormJSON();
                console.log(JSON.stringify(jsonData));
                console.log(jsonData) ;
                alert(jsonData);
                post("${pageContext.request.contextPath}/category/insert",JSON.stringify(jsonData),success);
            });
        } );
        function success(data){
            console.log(data);
            console.log("Success!!!");
        }
    </script>
    <div class="chagne">
        <ul class="pager">
            <%

            %>
            <li class="previous"><a href="/index/detail/${id-1}">Previous</a></li>
            <li class="next"><a href="/index/detail/${id+1}">Next</a></li>
            <%%>
        </ul>

    </div>
</div>

</body>
</html>
