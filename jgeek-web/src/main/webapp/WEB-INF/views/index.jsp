
<!DOCTYPE html>
<html>
<head>
    <title>index</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body >
<div class="container">
<div id="wrapper">
    <section>
        <div class="data-container"></div>
        <div id="pagination-demo1"></div>
    </section>
</div>

<script src="/resources/src/pagination.js"></script>
<script >
    // paging
    $(function () {
        var stringJson = $.getJSON("${pageContext.request.contextPath}/index/all");
        console.log(stringJson);
        var data;
        $.ajax({
            dataType: "json",
            url: "${pageContext.request.contextPath}/index/all",
            data: data,
            success: success
        });
        var items = [];

        $.getJSON( "${pageContext.request.contextPath}/index/all", function( data ) {

            $.each( data, function( key, val ) {
                function createDemo(name) {
                    var result = [];
                    var container = $('#pagination-' + name);
                    var sources = function () {

                        for(var i=0;i<val.length;i++){

                            result.push( "title  : <a href='/index/detail/"+val[i].id+"' target='_blank'> " +val[i].title
                                    +"</a><p></p><p></p> <p></p>  Content : " +val[i].conetnt);

                        }
                        return result;
                    }();
                    var options = {
                        dataSource: sources,
                        callback: function (response, pagination) {
                            window.console && console.log(response, pagination);
                            var dataHtml = '<ul>';
                            $.each(response, function (index, item) {
                                dataHtml += '<li>' + item + '</li>';
                            });
                            dataHtml += '</ul>';
                            container.prev().html(marked(dataHtml));
                        }
                    };

                    //$.pagination(container, options);

                    container.addHook('beforeInit', function () {
                        window.console && console.log('beforeInit...');
                    });
                    container.pagination(options);

                    container.addHook('beforePageOnClick', function () {
                        window.console && console.log('beforePageOnClick...');
                        //return false
                    });

                    return container;
                }
                createDemo('demo1');

            });
        });

    });
</script>
<jsp:include page="markdown.jsp"/>
</div>
</body>
</html>
