<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title><sitemesh:write property='title'/></title>
    <spring:url value="/resources/bootstrap/css/bootstrap.min.css" var="boostrapCss"/>
    <link href="${boostrapCss}" rel="stylesheet">

    <spring:url value="/resources/jquery.dataTables.min.css" var="jtableCss"/>
    <link href="${jtableCss}" rel="stylesheet">

    <spring:url value="/resources/jquery.min.js" var="jqueryJs"/>
    <script src="${jqueryJs}"></script>
    <spring:url value="/resources/mycustom.js" var="customJs"/>
    <script src="${customJs}"></script>
    <spring:url value="/resources/bootstrap/js/bootstrap.min.js" var="bootstrapJs"/>
    <script src="${bootstrapJs}"></script>

    <spring:url value="/resources/jquery.dataTables.min.js" var="datatableJs"/>
    <script src="${datatableJs}"></script>
    <spring:url value="/resources/bootstrap/MyJS.js" var="myjs"/>
    <script src="${myjs}"></script>
    <spring:url value="/resources/dist/pagination.css" var="myjs2"/>
    <link href="${myjs2}" rel="stylesheet" type="text/css">
    <spring:url value="/resources/markdown/markdown.css" var="myjs3"/>
    <link href="${myjs3}" rel="stylesheet" type="text/css">
    <sitemesh:write property='head'/>
</head>

<body>
<div id="jquery-script-menu" style="margin-bottom: 70px;border-bottom: 1px solid #9acfea" class="navbar navbar-fixed-top navbar-default" >
    <script type="text/javascript"
            src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
    </script>
    <div class="header clearfix" style="margin-top:5px; margin-left: 250px;">
        <nav style="float: left;">
            <ul class="nav nav-pills pull-right">
                <li role="presentation" class="active"><a href="/index">Home</a></li>
                <li role="presentation"><a href="/index/detail/1">Detail</a></li>
                <li role="presentation"><a href="#">Contact</a></li>
            </ul>
        </nav>
        <h3 class="text-muted" style="float:right;margin-right: 250px;">My Prodject </h3>
    </div>
</div>

<div style="margin-top: 150px;">
    <sitemesh:write property='body'/>
</div>

<div class="container">
    <footer class="footer" style="float: right;margin-top: 50px;">
        <p>&copy; 2016 Company, Inc.</p>
    </footer>
</div>




</body>
</html>
