package jgeek.web.constants;

/**
 * Created by luyenchu on 10/26/16.
 */
public interface ErrorConstant {
    int RESPONSE_ERROR = 100;
    int INVALID_PARAM = 101;
}
