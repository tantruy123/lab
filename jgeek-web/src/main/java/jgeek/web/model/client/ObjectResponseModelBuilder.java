package jgeek.web.model.client;

/**
 * Created by luyenchu on 10/26/16.
 */
public class ObjectResponseModelBuilder<T> {
    private T content;
   // private int errorCode;
  //  private String desc;

    private ObjectResponseModelBuilder() {
    }

    public static <T> ObjectResponseModelBuilder<T> anObjectResponseModel() {
        return new ObjectResponseModelBuilder<>();
    }

    public ObjectResponseModelBuilder withContent(T content) {
        this.content = content;
        return this;
    }

//    public ObjectResponseModelBuilder withErrorCode(int errorCode) {
//        this.errorCode = errorCode;
//        return this;
//    }
//
//    public ObjectResponseModelBuilder withDesc(String desc) {
//        this.desc = desc;
//        return this;
//    }

    public ObjectResponseModelBuilder but() {
        return anObjectResponseModel().withContent(content);//.withErrorCode(errorCode).withDesc(desc);
    }

    public ObjectResponseModel<T> build() {
        ObjectResponseModel<T> objectResponseModel = new ObjectResponseModel<T>();
        objectResponseModel.setContent(content);
       // objectResponseModel.setErrorCode(errorCode);
     //   objectResponseModel.setDesc(desc);
        return objectResponseModel;
    }
}
