package jgeek.web.model.client;

/**
 * Created by luyenchu on 10/26/16.
 */
public class ObjectResponseModel<T> {
  //  private int errorCode;
   // private String desc;
    private T content;
//
//    public String getDesc() {
//        return desc;
//    }

//    public void setDesc(String desc) {
//        this.desc = desc;
//    }

//    public int getErrorCode() {
//        return errorCode;
//    }
//
//    public void setErrorCode(int errorCode) {
//        this.errorCode = errorCode;
//    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }
}
