package jgeek.web.model.builder;

import jgeek.web.model.MySiteModel;

/**
 * Created by tan on 2016-10-28.
 */
public class MySiteBuilder {
    public String name;
    public int id;

    public String title;

    private MySiteBuilder() {
    }

    public static MySiteBuilder aCategoryModel() {
        return new MySiteBuilder();
    }

    public MySiteBuilder withName(String categoryName) {
        this.name = categoryName;
        return this;
    }

    public MySiteBuilder withId(int id) {
        this.id = id;
        return this;
    }
    public MySiteBuilder withTitle(String title){
        this.title=title;
        return this;
    }


    public MySiteBuilder but() {
        return aCategoryModel().withName(name).withId(id).withTitle(title);
    }

    public MySiteModel build() {
        MySiteModel mySiteModel = new MySiteModel();
        mySiteModel.conetnt=name;
        mySiteModel.id=id;
        mySiteModel.title=title;
        return mySiteModel;
    }
}
