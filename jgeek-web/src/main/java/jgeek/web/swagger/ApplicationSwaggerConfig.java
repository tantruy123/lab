package jgeek.web.swagger;

/**
 * Created by luyenchu on 10/27/16.
 */

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
public class ApplicationSwaggerConfig {
}
