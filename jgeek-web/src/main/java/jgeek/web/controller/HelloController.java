package jgeek.web.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

/**
 * Created by tan on 2016-10-28.
 */
@Controller
public class HelloController {
    @RequestMapping("/index")
    public String hello(ModelMap model) {
        model.addAttribute("date", new Date());

        return "index";

    }


}
