package jgeek.web.controller;


import jgeek.entities.MySite;
import jgeek.repo.MySiteRepository;
import jgeek.repo.MySiteRepositoryID;
import jgeek.web.model.MySiteModel;
import jgeek.web.model.builder.MySiteBuilder;


import jgeek.web.model.client.ObjectResponseModel;
import jgeek.web.model.client.ObjectResponseModelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by tan on 2016-10-28.
 */
@Controller
@RequestMapping("/index")
public class MySiteController {

    static final Logger logger = LoggerFactory.getLogger(MySiteController.class);

    @Autowired
    MySiteRepository mySiteRepository;

    @Autowired
    MySiteRepositoryID mySiteRepositoryID;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public ObjectResponseModel<List<MySiteModel>> allMySite() {
        ObjectResponseModelBuilder<List<MySiteModel>> builder = ObjectResponseModelBuilder.anObjectResponseModel();
        logger.info("All site repo: {}", mySiteRepository);
        try {

            Iterable<MySite> all = mySiteRepository.findAll();
            List<MySiteModel> mySiteModels = new ArrayList<>();
            all.forEach(mySite -> {
                MySiteBuilder mySiteBuilder = MySiteBuilder.aCategoryModel().withId(mySite.id).withName(mySite.content).withTitle(mySite.title);
                mySiteModels.add(mySiteBuilder.build());
            });
            builder.withContent(mySiteModels);
        } catch (Exception e) {
       //     builder.withErrorCode(ErrorConstant.RESPONSE_ERROR);
//            builder.withDesc("Error: " + e);
//            logger.error("Error", e);

        }
        logger.info("all {}", builder.build().toString());
        return builder.build();
    }


    @RequestMapping(value = "/insert", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public ObjectResponseModel<MySiteModel> insert(@RequestBody MySiteModel mySiteModel) {
        //validate
        ObjectResponseModelBuilder responseModelBuilder = ObjectResponseModelBuilder.anObjectResponseModel();
        try {
            MySite mySite = new MySite();
            mySite.content = mySiteModel.conetnt;
            mySite.title = "";
            mySiteRepository.save(mySite);

            MySiteModel resCatModel
                    = MySiteBuilder.aCategoryModel().withId(mySite.id).withName(mySite.content).withTitle(mySite.title).build();

            responseModelBuilder.withContent(resCatModel);
        } catch (Exception e) {
//            responseModelBuilder
//                    .withContent("Error: " + e);
        }
        return responseModelBuilder.build();
    }

    //    @RequestMapping(value = "/detail/{id}",method = RequestMethod.GET)
//    @ResponseBody
//    public ObjectResponseModel detailMySite(@PathVariable("id") String id , ModelMap modelMap){
//        MySiteModel mySiteModel= new MySiteModel();
//        ObjectResponseModelBuilder objectResponseModelBuilder =ObjectResponseModelBuilder.anObjectResponseModel();
//        try {
//            if(id.trim().equalsIgnoreCase("")){
//                throw new Exception();
//            }
//            id = id.replace("'","");
//            int mid = Integer.parseInt(id);
//            MySite mySite = mySiteRepositoryID.getProductByCategoryId(mid);
//
//            mySiteModel.conetnt=mySite.content;
//            mySiteModel.title=mySite.title;
//            mySiteModel.id=mySite.id;
//            objectResponseModelBuilder.withContent(mySiteModel);
////            modelMap.addAttribute("id",mySiteModel.id);
////            modelMap.addAttribute("title",mySiteModel.title);
////            modelMap.addAttribute("content",mySiteModel.conetnt);
////            modelMap.addAttribute("mysite",mySiteModel);
//        }catch (Exception e ){
//            objectResponseModelBuilder.withContent("Not found in Page ! Back to index Page");
//
//        }
//        return objectResponseModelBuilder.build();
//    }
    @RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
    public String detailMySite(@PathVariable("id") String id, ModelMap modelMap) {
        MySiteModel mySiteModel = new MySiteModel();

        try {
            if (id.trim().equalsIgnoreCase("")) {
                throw new Exception();
            }
            id = id.replace("'", "");
            int mid = Integer.parseInt(id);
            if(mid>mySiteRepositoryID.getMax() || mid<0){

                return  "error";
            }
            if(mid<=0){
                mid=mySiteRepositoryID.getMax();
            }else if(mid>=mySiteRepositoryID.getMax()+1){
                mid=1;
            }
            MySite mySite = mySiteRepositoryID.getProductByCategoryId(mid);
            mySiteModel.conetnt = mySite.content;
            mySiteModel.title = mySite.title;
            mySiteModel.id = mySite.id;
            modelMap.addAttribute("id", mySite.id);
            modelMap.addAttribute("title", mySite.title);
            modelMap.addAttribute("content", mySite.content);
            modelMap.addAttribute("mysite",mySite);
        } catch (Exception e) {
            modelMap.addAttribute("error", "Not found in Page ! Back to index Page");

        }
        return "detail";
    }

}
