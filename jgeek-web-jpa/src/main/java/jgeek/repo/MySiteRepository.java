package jgeek.repo;

import jgeek.entities.MySite;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by tan on 2016-10-28.
 */
@Repository
public interface MySiteRepository extends PagingAndSortingRepository<MySite,Integer> {

}
